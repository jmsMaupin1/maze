const map = [
    "WWWWWWWWWWWWWWWWWWWWW",
    "W   W     W     W W W",
    "W W W WWW WWWWW W W W",
    "W W W   W     W W   W",
    "W WWWWWWW W WWW W W W",
    "W         W     W W W",
    "W WWW WWWWW WWWWW W W",
    "W W   W   W W     W W",
    "W WWWWW W W W WWW W F",
    "S     W W W W W W WWW",
    "WWWWW W W W W W W W W",
    "W     W W W   W W W W",
    "W WWWWWWW WWWWW W W W",
    "W       W       W   W",
    "WWWWWWWWWWWWWWWWWWWWW",
];
const grid = document.getElementById('maze-grid');
let bTop = 0, bLeft = 0;

function createCell(type) {
    let cell = document.createElement('div');
    cell.classList.add('cell');
    if (type === 'W') cell.classList.add('wall');
    if (type === 'S') cell.classList.add('start');
    if (type === 'F') cell.classList.add('finish')

    return cell;
}

function createRow(id) {
    let row = document.createElement('div');
    row.id = id;
    row.classList.add('row');

    return row;
}

function drawBoard(map) {
    for(let y = 0; y < map.length; y++) {
        grid.appendChild(createRow(y));
        for(let x = 0; x < map[0].length; x++) {
            if(map[y][x] === 'S') {
                addPlayer(x,y);
                bTop = y * 60;
                bLeft = x * 00;
            }
            let cell = createCell(map[y][x]);
            document.getElementById(y).appendChild(cell);
        }
    }
}

function addPlayer(x, y) {
    let cell = document.createElement('div');
    cell.classList.add('player');
    cell.id = 'player'
    cell.style.top = `${y * 60}px`;
    cell.style.left = `${x * 60}px`;
    grid.appendChild(cell);
}

function checkMoves(x, y) {
    let moves = [
        {x:  0, y: -1},
        {x:  0, y:  1},
        {x: -1, y:  0},
        {x:  1, y:  0},
    ]

    return moves.map( move => {
        let newCoords = {
            x: x + move.x,
            y: y + move.y
        }

        return map[newCoords.y][newCoords.x] !== 'W'
            && map[newCoords.y][newCoords.x] !== undefined;
    })
}

function checkForWin(top, left) {
    let x = top / 60;
    let y = left / 60;

    if(map[x][y] === 'F') return true;
}

drawBoard(map);

document.addEventListener('keydown', e => {
    let availableMoves = checkMoves(bLeft / 60, bTop / 60);
    if(e.key === 'ArrowUp' && availableMoves[0])
        bTop -= 60;
    else if (e.key === 'ArrowDown' && availableMoves[1])
        bTop += 60;
    else if (e.key === 'ArrowLeft' && availableMoves[2])
        bLeft -= 60;
    else if (e.key === 'ArrowRight' && availableMoves[3])
        bLeft += 60;

    let player = document.getElementById('player');
    player.style.top = `${bTop}px`;
    player.style.left = `${bLeft}px`;

    if(checkForWin(bTop, bLeft)) {
        document.getElementById('modal').style.display = 'flex';
    }
})


document.getElementById('modal').addEventListener('click', e => {
    document.getElementById('modal').style.display = 'none'
})